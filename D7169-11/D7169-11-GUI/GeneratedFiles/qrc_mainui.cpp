/****************************************************************************
** Resource object code
**
** Created: Thu Jul 25 20:19:20 2013
**      by: The Resource Compiler for Qt version 4.8.4
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

QT_END_NAMESPACE


int QT_MANGLE_NAMESPACE(qInitResources_mainui)()
{
    return 1;
}

Q_CONSTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qInitResources_mainui))

int QT_MANGLE_NAMESPACE(qCleanupResources_mainui)()
{
    return 1;
}

Q_DESTRUCTOR_FUNCTION(QT_MANGLE_NAMESPACE(qCleanupResources_mainui))

