// stdafx.h - Precompiled Header
// Written by Jesse Z. Zhong
#ifndef __Precompiled_Header__
#define __Precompiled_Header__

// Qt
#include <QWidget>

// Custom plot
#include "qcustomplot.h"

// Standard C++
#include <string>
#include <vector>
#include <cmath>
#include <list>
#include <assert.h>
#include <iostream>

// Data simulation
#include <Simulation.h>

// Project utilities
#include "Util.h"

// NetCDF
#include <ncFile.h>
#include <ncDim.h>
#include <ncVar.h>
#include <ncAtt.h>

#endif // End : __Precompiled_Header__


